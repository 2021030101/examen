const limpiar = document.querySelector('.btn.limpiar');
const inputs = document.querySelectorAll('.inp:not(select)');

limpiar.addEventListener('click', e => {
    e.preventDefault();
    inputs.forEach(input => input.value = "");
});