const express = require('express');
const app = express();
const puerto = 80;
const router = require('./router/index');

app.set('view engine', 'ejs');
app.use(express.urlencoded({extended: false}));
app.use(express.static(__dirname + '/public'));

app.use(router);

app.listen(puerto);