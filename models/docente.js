class Docente {
    constructor(numero, nombre, domicilio, nivel, pago, horas, hijos) {
        this.numero = numero;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pago = pago;
        this.horas = horas;
        this.hijos = hijos;
    }

    get getPagoHora() {
        let pagoHoras = this.pago * this.horas;
        return this.nivel === 1 ? pagoHoras + pagoHoras * 0.30 :
               this.nivel === 2 ? pagoHoras + pagoHoras * 0.50 :
               this.nivel === 3 ? pagoHoras * 2 : 0;
    }

    get getImpuesto() {
        return this.getPagoHora * 0.16;
    }

    getBono() {
        return this.hijos === 1 ? this.getPagoHora * 0.05 :
               this.hijos === 2 ? this.getPagoHora * 0.10 :
               this.hijos === 3 ? this.getPagoHora * 0.20 : 0;
    }

    get getTotal() {
        return this.getPagoHora + 
               this.getBono(this.hijos) - 
               this.getImpuesto;
    }
}

module.exports = Docente;