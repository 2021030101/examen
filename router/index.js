const router = require('express').Router();

const Docente = require('../models/docente.js');

router.get('/inicio', (req, res) => {
    res.render('index', {
        docente: null,
    });
});

router.post('/inicio', (req, res) => {
    const obj = new Docente(parseInt(req.body.numDocente),
                            req.body.nombre,
                            req.body.domicilio,
                            parseInt(req.body.nivel),
                            parseInt(req.body.pago),
                            parseInt(req.body.horas),
                            parseInt(req.body.hijos));
    res.render('index', {
        docente: obj || null,
    });
});

module.exports = router;